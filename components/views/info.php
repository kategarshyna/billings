<a href="#" data-toggle="tooltip" data-placement="bottom" data-html="true"
   title="Created: <?=date('l jS \of F Y h:i:s A', $model->created_at) ?>
   Updated: <?= date('l jS \of F Y h:i:s A', $model->updated_at) ?>">
	<i class="fa fa-info-circle text-info"></i></a>
