<?php

use app\models\Category;
use app\models\StaticPage;
use yii\helpers\Html;

/** @var Category $category */

?>
<div class="navbar navbar-inverse" role="banner">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="/">
                <h1><img src="/images/logo.png" alt="logo"></h1>
            </a>

        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><?= Html::a('Billings', ['/']) ?></li>
	            <li><?= Html::a('Add New Bill', ['site/create-bill']) ?></li>
            </ul>
        </div>
    </div>
</div>
