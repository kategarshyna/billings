<?php
namespace app\components;

use yii\base\Widget;

class InfoWidget extends Widget
{
    public $model;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        parent::run();

        if ($this->model == null) {
            return '';
        }
        return $this->render('info', [
            'model' => $this->model
            ]);
    }
}