<?php

use yii\db\Migration;

class m171024_063822_create_table_bill extends Migration
{
	public function safeUp()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%bills}}', [
			'id' => $this->integer(11)->notNull()->append('AUTO_INCREMENT PRIMARY KEY'),
			'vendor' => $this->string(255),
			'price' => $this->string(255),
			'is_paid' => $this->smallInteger(4),
			'created_at' => $this->integer(11),
			'updated_at' => $this->integer(11),
			'category_id' => $this->integer(11),
		], $tableOptions);

		$this->addForeignKey('FK__categories', '{{%bills}}', 'category_id', '{{%categories}}', 'id');
	}

	public function safeDown()
	{
		$this->dropTable('{{%bills}}');
	}
}
