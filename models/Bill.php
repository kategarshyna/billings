<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "posts".
 *
 * @property integer $id
 * @property string $vendor
 * @property string $price
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $category_id
 * @property bool $is_paid

 *
 * @property Category $category
 */
class Bill extends AdminBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bills';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vendor', 'price', 'category_id'], 'required'],
            [['is_paid', 'created_at', 'updated_at', 'category_id'], 'integer'],
            [['vendor'], 'string', 'max' => 255],
            [['price'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(),
                'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vendor' => 'Vendor',
            'price' => 'Price',
            'created_at' => 'Date',
            'updated_at' => 'Updated At',
            'category_id' => 'Category',
            'is_paid' => 'Is Paid',
        ];
    }
      /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
