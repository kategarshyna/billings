<?php

$urls = [
	"/example/example",
];

$file = __DIR__ . '/request_filter.txt';
$urls = file_exists($file) ? array_merge($urls, file($file, FILE_SKIP_EMPTY_LINES|FILE_IGNORE_NEW_LINES)) : $urls;
$urls = array_filter($urls);

$requestUrl = rtrim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');

foreach ($urls as $key => $url) {
	if (rtrim($url, '/') === $requestUrl && !empty($urls)) {
		http_response_code(404);
		echo '<html><head><title>404 Not Found</title></head><body bgcolor="white"><center><h1>404 Not Found</h1></center><hr><center>nginx</center></body></html>';
		die;
	}
}
