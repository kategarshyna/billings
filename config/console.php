<?php

use yii\helpers\ArrayHelper;

$config = ArrayHelper::merge(require (__DIR__ . '/common.php'), [
    'id' => 'basic-console',
    'controllerNamespace' => 'app\commands',
    'controllerMap' => [
        'migration' => [
            'class' => 'bizley\migration\controllers\MigrationController',
        ],
    ],
]);

return $config;
