<?php

use webvimark\modules\UserManagement\models\UserVisitLog;
use yii\helpers\ArrayHelper;

$config = ArrayHelper::merge( require (__DIR__ . '/common.php') ,[
    'id' => 'basic',
    'components' => [
        'request' => [
            'cookieValidationKey' => '5cIcv6wvhC4us_UYLIOTRxGbTZZShARc',
        ],
        'user' => [
	        'identityClass' => 'app\models\User',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'assetManager' => [
            'class' => '\yii\web\AssetManager',
            'dirMode' => 0777,
            'fileMode' => 0666,
            'appendTimestamp' => true,
            'linkAssets' => true,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
    ],
]);

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['178.150.65.46'],
        'dirMode' => 0777,
        'fileMode' => 0666,
    ];
}

return $config;
