<?php

use yii\helpers\ArrayHelper;

$config = [
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'dirMode' => 0777,
            'fileMode' => 0666,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                    'class' => 'Swift_SmtpTransport',
                    'port' => '465',
                    'encryption' => 'ssl',
             ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                'main' => [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/all-levels.log',
                    'levels' => ['error', 'warning'],
                    'except' => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'yii\web\ForbiddenHttpException'
                    ],
                    'logVars' => [],
                    'dirMode' => 0777,
                    'fileMode' => 0666,
                ],
                'email' => [
                    'class' => 'yii\log\EmailTarget',
                    'levels' => ['error', 'warning'],
                    'except' => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'yii\web\ForbiddenHttpException',
                    ],
                    'logVars' => [],
                    'message' => [
                        'subject' => 'Billing error log'
                    ],
                ]
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'charset' => 'utf8',
        ],
        'i18n' => [
            'translations' => [
                'yii2mod.comments' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@yii2mod/comments/messages',
                ],
            ],
        ],
    ],
    'params' => [
        'siteName' => 'Billing Test Site',
    ],
];


$pathLocal = __DIR__ . '/common.local.php';
if(file_exists($pathLocal)) {
    $config = ArrayHelper::merge($config, require $pathLocal);
}

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['178.150.65.46'],
    ];
}

return $config;
