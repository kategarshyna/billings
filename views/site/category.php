<?php

use yii\widgets\ListView;

/** @var \yii\debug\models\timeline\DataProvider $categoryPosts */
/** @var string $categoryName */

$this->title = $categoryName;

?>
<h3>All posts by Category: <strong><?= $categoryName?></strong></h3>
<div class="row">
    <?= ListView::widget([
        'dataProvider' => $categoryPosts,
        'pager' => [
            'hideOnSinglePage' => true,
        ],
        'layout' => '{items} <div class="blog-pagination">{pager}</div>',
        'summary'=>'',
        'itemView' => '_short_post_view'
    ])?>
</div>