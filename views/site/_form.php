<?php

use app\models\Category;
use app\models\Tag;
use dosamigos\selectize\SelectizeDropDownList;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\redactor\widgets\Redactor;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="bill-form">

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>

    <div class="col-xs-12 col-md-6">

        <?= $form->field($model, 'vendor')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'is_paid')->checkbox() ?>

        <?= $form->field($model, 'category_id')
            ->dropDownList(ArrayHelper::map(Category::find()->all(), 'id', 'name'), [
                'prompt' => 'Select post category'
            ]) ?>
    </div>

    <div class="form-group col-xs-12">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ?
            'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
