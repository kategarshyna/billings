<?php

use app\components\InfoWidget;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bill */

$this->title = 'Update Bill: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Bills', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bill-update">

    <h1><?= Html::encode($this->title) ?> <?= $model->isNewRecord ? '' : InfoWidget::widget(['model' => $model])?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
