<?php

/** @var \app\models\Bill $model */
use app\models\Bill;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<div class="col-sm-12 col-md-12">
    <div class="single-blog single-column">
        <div class="post-thumb">
            <a href="blogdetails.html">
                <!--                <img src="images/blog/7.jpg" class="img-responsive" alt="">-->
            </a>
            <div class="post-overlay">
                <span class="uppercase"><a href="#">14 <br><small>Feb</small></a></span>
            </div>
        </div>
        <div class="post-content overflow">
            <h2 class="post-title bold"><?= Html::a($model->id, [
                    'site/bill',
                    'id' => $model->id
                ]) ?></h2>
	        <span class="label <?= $model->is_paid ? 'label-success' : 'label-danger'?>"> <?= $model->is_paid ? 'Paid' : 'Not Paid'?></span>
            <h3 class="post-author">Date <?= date('l jS \of F Y h:i:s A', $model->created_at) ?>
            </h3>
            <p>Vendor:<?= $model->vendor ?></p>
            <p>Price: <?= $model->price ?></p>
            <?= Html::a('Update', ['site/update-bill', 'id' => $model->id],
                ['class' => 'read-more']) ?>
            <div class="post-bottom overflow">
                <ul class="nav navbar-nav post-nav">
                    <li><?= Html::a('<i class="fa fa-list-ul"></i>' . $model->category->name,
                            ['#']) ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
